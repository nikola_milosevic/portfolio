$(document).ready(function() {

	//owlCarousel
	$("#skills-list").owlCarousel ({
		items : 8,
		lazyLoad : true,
		autoPlay : true,
		speed: 900,
		pagination : false,
	});

	//FancyBox
	$('.fancybox').fancybox({
		fitToView	: false,
		width		: '95%',
		height		: '95%',
		autoSize	: false,
		closeClick	: false,
		openEffect	: 'none',
		closeEffect	: 'fade',
		closeBtn	: true,
	});

	//GoToTop
	$(function(){
        $(".gototop").gototop({
            position : 0,
            duration : 1250,
            visibleAt : 500,
            classname : "isvisible"
        });
    });

	//ScrollDown
	$('a').click(function(){
    $('html, body').animate({
        scrollTop: $( $(this).attr('href') ).offset().top
    }, 1000);
    return false;
	});

    //ContactForm
    $('#contactForm').on('submit', function() {
        
        var that = $(this),
            url = that.attr('action'),
            method = that.attr('method'),
            data = {};

        that.find('[name]').each(function(index, value) {

            var that = $(this),
                name = that.attr('name'),
                value = that.val();

            data[name] = value;
        });

        $.ajax({
            url: url,
            type: method,
            data: data,
            success: function (data) {
                if(data==='fail'){
                    $("#msgError").fadeIn('slow').delay(2000).fadeOut('slow'); 
                }else{
                    $("#msgSuccess").fadeIn('slow').delay(2000).fadeOut('slow');
                    that.find('[name]').val('');
                }
            }
        });

        return false;
    });

});